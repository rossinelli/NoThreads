export ISPC=/c/ispc-v1.9.1-windows-vs2015/ispc.exe
#export ISPCFLAGS=" --target=avx2-i32x8,avx1-i32x8 "
#export ISPCFLAGS=" --target=avx1-i32x8 "

export CXX="g++ "
export MPILIBS=" -L${HOME}/ms-mpi/lib/x64/ -lmsmpi "
#export CXXFLAGS="-static -static-libgcc -static-libstdc++ -march=native -mtune=native"
export CXXFLAGS=" -I${HOME}/ms-mpi/include/ -static -static-libgcc -static-libstdc++ -march=native -mtune=native"
export CCFLAGS="-static -static-libgcc  -mtune=native -march=native "
#export CXXFLAGS="-static -static-libgcc -static-libstdc++ -march=sandybridge -mtune=sandybridge"


#export CCFLAGS=" -Ofast -fstrict-aliasing -DNDEBUG "
#export GLEW_INC=/home/diego/glew/include/
#Exportsh GLEW_LIB="-L/home/diego/glew/lib -lGLEW  -lopengl32 -lgdi32"
#export OMP_LIB=" /mingw64/lib/gcc/x86_64-w64-mingw32/6.2.0/libgomp.a "
