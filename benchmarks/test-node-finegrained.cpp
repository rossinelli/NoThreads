#include <cstring>
#include <cassert>
#include <stdint.h>

#include <nothreads.h>
#include <nothreads-utils.h>

using nothreads::node;
using nothreads::numa;
using nothreads::core;
using nothreads::hwt;

void test_write(bool RR)
{
    const int ntimes = 100;
    const int xsize = 1920, ysize = 1920, zsize = 64;
    const int xysize = xsize * ysize;
    const size_t nbytes = xsize * ysize * (size_t)zsize * sizeof(int16_t);
    
    //ALLOCATE MEMORY SHARED BY HW THREADS OF THE SAME NODE
    int16_t * data = (int16_t *)nothreads::shmem_alloc(numa, nbytes);

    //THE BENCHMARK
    MPI_CHECK(MPI_Barrier(numa.comm));

    const double t0 = MPI_Wtime();

    int zs, ze;
    nothreads::divide_workload(numa, zsize, zs, ze);

    int ys, ye;
    nothreads::divide_workload(core, ysize, ys, ye);

    int xs, xe;
    nothreads::divide_workload(hwt, xsize, xs, xe);

    MPI_CHECK(MPI_Barrier(numa.comm));

    if (RR)
    {
	for(int t = 0; t < ntimes; ++t)
	    for(int zi = numa.id; zi < zsize; zi += numa.count)
	      for(int yi = core.id; yi < ysize; yi += core.count)
		memset(data + xs + xsize * (yi + ysize * zi), 
		       zi % core.count, sizeof(*data) * (xe - xs));
    }
    else
    {
	for(int t = 0; t < ntimes; ++t)
	    for(int zi = zs; zi < ze; ++zi)
	      for(int yi = ys; yi < ye; ++yi)
		memset(data + xs + xsize * (yi + ysize * zi), 
		       zi % core.count, sizeof(*data) * (xe - xs));
    }

    MPI_CHECK(MPI_Barrier(numa.comm));

    const double t1 = MPI_Wtime();

    //VERIFY AND COMPUTE PERFORMANCE
    double aggrwbw = 0;

    if (hwt.id == 0 && core.id == 0 && numa.id == 0)
	aggrwbw = ntimes * (sizeof(*data) * xysize * (size_t)zsize) / 1e9 / (t1 - t0);

    //REPORT PERFORMANCE
    MPI_CHECK(MPI_Allreduce(MPI_IN_PLACE, &aggrwbw, 1, MPI_DOUBLE, MPI_SUM, node.comm));

    if (node.id == 0 && numa.id == 0 && core.id == 0 && hwt.id == 0)
	printf("RR: %d, aggregate WRITE bandwidth: %.3f GB/s, footprint: %.3f MB\n", RR, aggrwbw, nbytes / 1024. / 1024.);

    fflush(stdout);

    int16_t * dest = (int16_t *)nothreads::shmem_alloc(numa, nbytes);

    MPI_CHECK(MPI_Barrier(numa.comm));

    const double t2 = MPI_Wtime();

    MPI_CHECK(MPI_Barrier(numa.comm));

    if (RR)
    {
	for(int t = 0; t < ntimes; ++t)
	    for(int zi = core.id; zi < zsize; zi += core.count)
	      for(int yi = numa.id; yi < ysize; yi += numa.count)
		memcpy(dest + xs + xsize * (yi + ysize * zi),
		       data + xs + xsize * (yi + ysize * zi), sizeof(*data) * (xe - xs));
    }
    else
    {
	for(int t = 0; t < ntimes; ++t)
	    for(int zi = zs; zi < ze; ++zi)
	      for(int yi = ys; yi < ye; ++yi)
		memcpy(dest + xs + xsize * (yi + ysize * zi),
		       data + xs + xsize * (yi + ysize * zi), sizeof(*data) * (xe - xs));
    }

    MPI_CHECK(MPI_Barrier(numa.comm));

    const double t3 = MPI_Wtime();

    aggrwbw = 0;

    if (hwt.id == 0 && core.id == 0 && numa.id == 0)
    {
	for(int zi = 0; zi < zsize; ++zi)
	{
	    union { int16_t v16; uint8_t v8[2]; } val;

	    val.v8[0] = val.v8[1] = zi % core.count;

	    const int16_t * const slice = dest + xysize * zi;
	    //`const int16_t * const slice = data + xysize * zi;

	    for(int i = 0; i < xysize; ++i)
	    {
		if (slice[i] != val.v16)
		  printf("slice: x=%d y=%d val: %d %x vs %x\n", zi, i % xsize, i / xsize, slice[i], val.v16);

		assert(slice[i] == val.v16);
	    }
	}

	aggrwbw = ntimes * (2 * sizeof(*data) * xysize * (size_t)zsize) / 1e9 / (t3 - t2);
    }

    //REPORT PERFORMANCE
    MPI_CHECK(MPI_Allreduce(MPI_IN_PLACE, &aggrwbw, 1, MPI_DOUBLE, MPI_SUM, node.comm));

    if (node.id == 0 && numa.id == 0 && core.id == 0 && hwt.id == 0)
	printf("RR: %d, aggregate COPY bandwidth: %.3f GB/s, footprint: %.3f MB\n", RR, aggrwbw, 2 * nbytes / 1024. / 1024.);

    nothreads::shmem_free(dest);
    nothreads::shmem_free(data);
}

int main(int argc, char ** argv)
{
    MPI_CHECK(MPI_Init(&argc, &argv));

    nothreads::init();
    nothreads::batch();

    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));

    if (node.id == 0 && numa.id == 0 && core.id == 0 && hwt.id == 0)
    {
	printf("TEST NODE -- FINE GRAINED INTERLEAVING\n");

	fflush(stdout);
    }

    test_write(false);
    test_write(true);

    MPI_CHECK(MPI_Finalize());

    return 0;
}
