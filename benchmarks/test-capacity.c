#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include <mpi.h>

#include <nt.h>

#ifndef MPI_CHECK
#define MPI_CHECK(stmt)                                         \
    do                                                          \
    {                                                           \
	const int code = stmt;                                  \
                                                                \
	if (code != MPI_SUCCESS)                                \
	{                                                       \
	    char error_string[2048];                            \
	    int length_of_error_string = sizeof(error_string);  \
	    MPI_Error_string(code, error_string,                \
			     &length_of_error_string);          \
                                                                \
	    fprintf(stderr,                                     \
		    "ERROR!\n" #stmt " mpiAssert: %s %d %s\n",  \
		    __FILE__, __LINE__, error_string);          \
	    fflush(stderr);                                     \
                                                                \
	    MPI_Abort(MPI_COMM_WORLD, code);                    \
	}                                                       \
    }                                                           \
    while(0)
#endif

int main (
    int argc,
    char * argv[])
{
    MPI_CHECK(MPI_Init(&argc, &argv));

    nt_init();
    nt_batch();

    int nranks, rank;
    MPI_CHECK(MPI_Comm_size(MPI_COMM_WORLD, &nranks));
    MPI_CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank));

    const int master = rank == 0;

    int lr, lrn;
    MPI_CHECK(MPI_Comm_size(nt_core.comm, &lrn));
    MPI_CHECK(MPI_Comm_rank(nt_core.comm, &lr));

    if (argc != 4)
    {
	if (master)
	    fprintf(stderr,
		    "usage: %s <xsize> <ysize> <zsize>\n",
		    argv[0]);

	return master ? EXIT_FAILURE : EXIT_SUCCESS;
    }

    const ptrdiff_t xn = atoi(argv[1]);
    const ptrdiff_t yn = atoi(argv[2]);
    const ptrdiff_t yxn = yn * xn;

    const ptrdiff_t zn = atoi(argv[3]);

    float * slices[zn];
    memset(slices, 0, sizeof(slices));

    for(int i = 0; i < zn; ++i)
	slices[i] = nt_shmem(nt_core, sizeof(float) * yxn);

    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));

    for(int i = lr; i < zn; i += lrn)
	memset(slices[i], 0, sizeof(float) * yxn);

    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));

    for(int i = 0; i < zn; ++i)
    {
	nt_free(slices[i]);
	slices[i] = 0;
    }

    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));

    if (master)
	fprintf(stderr, "all done. bye.\n");

    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));

    MPI_CHECK(MPI_Finalize());

    return EXIT_SUCCESS;
}
