#include <cstring>
#include <cassert>
#include <stdint.h>

#include <algorithm>

#include <xmmintrin.h>

#include <nothreads.h>
#include <nothreads-utils.h>

using nothreads::node;
using nothreads::numa;
using nothreads::core;
using nothreads::hwt;

int main(int argc, char ** argv)
{
    MPI_CHECK(MPI_Init(&argc, &argv));

    nothreads::init();
    nothreads::batch();

    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));

    const bool verbose = hwt.global_id == 0;

    if (argc != 5)
    {
	if (verbose)
	    fprintf(stderr,
		    "usage: %s <path/to/raw> <xsize> <ysize> <zsize>\n",
		    argv[0]);

	MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));
	
	MPI_CHECK(MPI_Finalize());

	return EXIT_FAILURE;
    }
        
    if (verbose)
	printf("=============== TEST IO READ AT NODE LEVEL ===============\n");

    fflush(stdout);

    const char * fname = argv[1];
    
    const int xsize = atoi(argv[2]), ysize = atoi(argv[3]), zsize = atoi(argv[4]);
    const int xysize = xsize * ysize;
    const int xtilesize = 32, ytilesize = 32, ztilesize = 32;
    const int tilesize = xtilesize * ytilesize * ztilesize;
    const int xntiles = (xsize + xtilesize - 1) / xtilesize;
    const int yntiles = (ysize + ytilesize - 1) / ytilesize;
    const int xyntiles = xntiles * yntiles;
    const int zntiles = (zsize + ztilesize - 1) / ztilesize;
    
    const size_t readnbytes = xysize * ztilesize * sizeof(int16_t);
    int16_t * const readbuffer = (int16_t *)nothreads::shmem_alloc(core, readnbytes);

    int16_t * srcbuf = (int16_t *)_mm_malloc(tilesize * sizeof(int16_t), 32);
    
    if (verbose)
	printf("allocating read buffer of %.3f MiB\n", readnbytes / 1024. / 1024.);

    MPI_File fin;
    MPI_CHECK(MPI_File_open(core.comm, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &fin));
    SYS_CHECK(fin != NULL, "checking fin");
        
    int wid, nw;
    MPI_CHECK(MPI_Comm_size(core.comm, &nw));
    MPI_CHECK(MPI_Comm_rank(core.comm, &wid));

    MPI_CHECK(MPI_Barrier(node.comm));

    const double t0 = MPI_Wtime();

    MPI_CHECK(MPI_Barrier(node.comm));
    
    int node_zs, node_ze;
    nothreads::divide_workload(node, zntiles, node_zs, node_ze);

    int numa_zs, numa_ze;
    nothreads::divide_workload(numa, node_ze - node_zs, numa_zs, numa_ze);
    numa_zs += node_zs;
    numa_ze += node_zs;

    int64_t sum = 0, count = 0;

    size_t read = 0;
    
    for(int zt = numa_zs; zt < numa_ze; ++zt)
    {
	//read the slices into the read buffer
	const int zstart = zt * ztilesize;
	const int zn = std::max(0, std::min(zsize - zstart, ztilesize));
	
	int zs, ze;
	nothreads::divide_workload(wid, nw, zn, zs, ze);
	
	MPI_Status status;
	MPI_CHECK(MPI_File_read_at_all(fin,
				       (size_t)xysize * sizeof(int16_t) * (size_t)(zstart + zs),
				       readbuffer + (size_t)xysize * zs,
				       xysize * (ze - zs), MPI_SHORT, &status));

	read += xysize * (ze - zs) * sizeof(int16_t);
	
	MPI_CHECK(MPI_Barrier(core.comm));

	for(int xyt = wid; xyt < xyntiles; xyt += nw)
	{
	    const int xt = xyt % xntiles;
	    const int yt = xyt / xntiles;

	    const int xstart = xt * xtilesize;
	    const int ystart = yt * ytilesize;
		
	    const int xn = std::min(xsize - xstart, xtilesize);
	    const int yn = std::min(ysize - ystart, ytilesize);
	    
	    //copy my block
	    for(int z = 0; z < zn; ++z)
		for(int y = 0; y < yn; ++y)
		    memcpy(srcbuf + xtilesize * (y + ytilesize * z),
			   readbuffer + xstart + xsize * (y + ystart + ysize * z),
			   sizeof(int16_t) * xn);

	    int lsum = 0;
	    for(int zi = 0; zi < zn; ++zi)
		for(int yi = 0; yi < yn; ++yi)
		    for(int xi = 0; xi < xn; ++xi)
		    {
			/*const int16_t val = srcbuf[xi + xtilesize * (yi + ytilesize * zi)];
			  if (!(val == 0 || val == 257 || val == 514 || val == 771))
			    printf("zt: %d xyt: %d srcbuf[%d] = %d\n", zt,
				   xyt, xi + xtilesize * (yi + ytilesize * zi), srcbuf[xi + xtilesize * (yi + ytilesize * zi)]);
			
				   assert(val == 0 || val == 257 || val == 514 || val == 771);*/
			lsum += srcbuf[xi + xtilesize * (yi + ytilesize * zi)];
		    }

	    sum += lsum;
	    count += zn * yn * xn;
	}

	MPI_CHECK(MPI_Barrier(core.comm));
    }

    MPI_CHECK(MPI_File_close(&fin));

    MPI_Allreduce(MPI_IN_PLACE, &read, 1, MPI_INT64_T, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE, &sum, 1, MPI_INT64_T, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE, &count, 1, MPI_INT64_T, MPI_SUM, MPI_COMM_WORLD);
    
    if (verbose)
	printf("Read %zd bytes overall\n", read);
    
    /*double avg = sum / (double)count;
    MPI_Allreduce(MPI_IN_PLACE, &avg, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    {
	int nranks;
	MPI_CHECK(MPI_Comm_size(MPI_COMM_WORLD, &nranks));
	avg /= nranks;
	}*/

    const double avg = sum / (double)count;

    if (verbose)
	printf("Average voxel is %.3f\n", avg);
    
    MPI_CHECK(MPI_Barrier(node.comm));

    const double t1 = MPI_Wtime();

    const size_t overallfp = sizeof(int16_t) * xysize * (size_t)zsize;

    if (verbose)
     	printf("%.3fs, %zd bytes, %.3f GB/s\n", t1 - t0, overallfp, overallfp / 1e9 / (t1 - t0));
    
    _mm_free(srcbuf);
    
    nothreads::shmem_free(readbuffer);
    
    MPI_CHECK(MPI_Finalize());

    return 0;
}
