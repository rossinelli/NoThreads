#include <cstring>
#include <cassert>
#include <stdint.h>

#include <nothreads.h>
#include <nothreads-utils.h>

using nothreads::node;
using nothreads::numa;
using nothreads::core;
using nothreads::hwt;

void test_write(bool RR)
{
    const int ntimes = 10000;
    const int xsize = 32, ysize = 32, zsize = 1 * 32 * 32;
    const int xysize = xsize * ysize;
    const size_t nbytes = xsize * ysize * (size_t)zsize * sizeof(int16_t);

    //ALLOCATE MEMORY SHARED BY HW THREADS OF THE SAME CORE
    int16_t * data = (int16_t *)nothreads::shmem_alloc(hwt, nbytes);

    //THE BENCHMARK
    MPI_CHECK(MPI_Barrier(hwt.comm));
    
    const double t0 = MPI_Wtime();
    
    if (RR)
	for(int t = 0; t < ntimes; ++t)
	    for(int zi = hwt.id; zi < zsize; zi += hwt.count)
		memset(data + xysize * zi, zi % hwt.count, sizeof(*data) * xysize);
    else
    {
	int s, e;
	nothreads::divide_workload(hwt, zsize, s, e);
	
	for(int t = 0; t < ntimes; ++t)
	    for(int zi = s; zi < e; ++zi)
		memset(data + xysize * zi, zi % hwt.count, sizeof(*data) * xysize);
    }
        
    MPI_CHECK(MPI_Barrier(hwt.comm));

    const double t1 = MPI_Wtime();

    //VERIFY AND COMPUTE PERFORMANCE
    double aggrwbw = 0;
    
    if (hwt.id == 0)
    {
	for(int zi = 0; zi < zsize; ++zi)
	{
	    union { int16_t v16; uint8_t v8[2]; } val;
		
	    val.v8[0] = val.v8[1] = zi % hwt.count;
		
	    const int16_t * const slice = data + xysize * zi;

	    for(int i = 0; i < xysize; ++i)
	    {
		if (slice[i] != val.v16)
		    printf("val: %d %x vs %x\n", i, slice[i], val.v16);
		
		assert(slice[i] == val.v16);
	    }
	}

	aggrwbw = ntimes * (sizeof(*data) * xysize * (size_t)zsize) / 1e9 / (t1 - t0);
	//printf("verified. write throughput: %.3f GB/s\n", aggrwbw);
    }

    //REPORT PERFORMANCE
    MPI_CHECK(MPI_Allreduce(MPI_IN_PLACE, &aggrwbw, 1, MPI_DOUBLE, MPI_SUM, node.comm));

    if (node.id == 0 && numa.id == 0 && core.id == 0 && hwt.id == 0)
	printf("RR: %d, aggregate bandwidth: %.3f GB/s\n", RR, aggrwbw);

    nothreads::shmem_free(data);
}

int main(int argc, char ** argv)
{
    MPI_CHECK(MPI_Init(&argc, &argv));

    int rank;
    MPI_CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank));

    nothreads::init();
    nothreads::batch();

    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));
    
    printf("hello from globalrank %03d pe at node %d, numa %d, core %d, hw thread %d\n",
	   rank, node.id, numa.id, core.id, hwt.id);

    fflush(stdout);

    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));
    
    test_write(false);
    test_write(true);
    
    MPI_CHECK(MPI_Finalize());

    return 0;
}

