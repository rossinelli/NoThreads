#include <cstring>
#include <cassert>
#include <stdint.h>

#include <algorithm>

#include <xmmintrin.h>

#include <nothreads.h>
#include <nothreads-utils.h>

using nothreads::node;
using nothreads::numa;
using nothreads::core;
using nothreads::hwt;

int main(int argc, char ** argv)
{
    MPI_CHECK(MPI_Init(&argc, &argv));

    nothreads::init();
    nothreads::batch();

    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));

    const bool verbose = hwt.global_id == 0;
    
    if (verbose)
    {
	printf("=============== TEST IO WRITE AT NODE LEVEL ===============\n");

	fflush(stdout);
    }

    int zsize = 100;
    
    if (argc > 1)
	zsize = atoi(argv[1]);

    const int xsize = 1920, ysize = 1920; //, zsize = 2000 * 2;
    const int xysize = xsize * ysize;
    const int xtilesize = 32, ytilesize = 32, ztilesize = 32;
    const int tilesize = xtilesize * ytilesize * ztilesize;
    const int xntiles = (xsize + xtilesize - 1) / xtilesize;
    const int yntiles = (ysize + ytilesize - 1) / ytilesize;
    const int xyntiles = xntiles * yntiles;
    const int zntiles = (zsize + ztilesize - 1) / ztilesize;
    const int ntiles = xntiles * yntiles * zntiles;
    
    const size_t readnbytes = xysize * ztilesize * sizeof(int16_t);
    int16_t * const readbuffer = (int16_t *)nothreads::shmem_alloc(core, readnbytes);

    const size_t writenbytes = xntiles * yntiles * tilesize * sizeof(int16_t);
    uint8_t * const writebuffer = (uint8_t*)nothreads::shmem_alloc(core, writenbytes);

    const size_t metanbytes = sizeof(size_t) * (1 + xyntiles);
    int32_t * const metabuffer = (int32_t *)nothreads::shmem_alloc(core, metanbytes);
    int32_t * bytes_tail = metabuffer;

    int16_t * srcbuf = (int16_t *)_mm_malloc(tilesize * sizeof(int16_t), 32);
    uint8_t * dstbuf = (uint8_t *)_mm_malloc(tilesize * sizeof(int16_t), 32);
    
    if (verbose)
    {
	printf("allocating read buffer of %.3f MiB\n", readnbytes / 1024. / 1024.);
	printf("allocating write buffer of %.3f MiB\n", writenbytes / 1024. / 1024.);
	printf("allocating meta buffer of %.3f MiB\n", metanbytes / 1024. / 1024.);
    }

    MPI_File fout;
    char foutname[2048];
    sprintf(foutname, "data-part%d.raw", numa.global_id);
    MPI_CHECK(MPI_File_open(core.comm, foutname, MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &fout));
    SYS_CHECK(fout != NULL, "checking fout");
    
    MPI_CHECK(MPI_File_set_size(fout, ntiles * (size_t)tilesize * sizeof(int16_t)));
    
    int wid, nw;
    MPI_CHECK(MPI_Comm_size(core.comm, &nw));
    MPI_CHECK(MPI_Comm_rank(core.comm, &wid));

    MPI_CHECK(MPI_Barrier(node.comm));

    const double t0 = MPI_Wtime();

    MPI_CHECK(MPI_Barrier(node.comm));

    size_t allnbytes = 0;

    int node_zs, node_ze;
    nothreads::divide_workload(node, zntiles, node_zs, node_ze);

    int numa_zs, numa_ze;
    nothreads::divide_workload(numa, node_ze - node_zs, numa_zs, numa_ze);
    numa_zs += node_zs;
    numa_ze += node_zs;
    
    for(int zt = numa_zs; zt < numa_ze; ++zt)
    {
	//read the slices into the read buffer
	const int zstart = zt * ztilesize;
	const int zn = std::max(0, std::min(zsize - zstart, ztilesize));

	int zs, ze;
	nothreads::divide_workload(wid, nw, zn, zs, ze);
	for(int zi = zs; zi < ze; ++zi)
	    memset(readbuffer + xysize * zi, core.id, sizeof(*readbuffer) * xysize);

	if (wid == 0)
	    *bytes_tail = 0;
	
	MPI_CHECK(MPI_Barrier(core.comm));

	for(int xyt = wid; xyt < xyntiles; xyt += nw)
	{
	    const int xt = xyt % xntiles;
	    const int yt = xyt / xntiles;

	    //copy my block
	    {
		const int xstart = xt * xtilesize;
		const int ystart = yt * ytilesize;

		const int xn = std::min(xsize - xstart, xtilesize);
		const int yn = std::min(ysize - ystart, ytilesize);
		
		for(int z = 0; z < zn; ++z)
		    for(int y = 0; y < yn; ++y)
			memcpy(srcbuf + xtilesize * (y + ytilesize * z),
			       readbuffer + xstart + xsize * (y + ystart + ysize * z),
			       sizeof(int16_t) * xn);
	    }

	    //do output = f(input)	
	    const size_t outnbytes = sizeof(int16_t) * tilesize;
	    memcpy(dstbuf, srcbuf, outnbytes);

	    //aquire a slot
	    const int32_t mypos = nothreads::fetch_and_add(bytes_tail, outnbytes);
	    assert((size_t)mypos < writenbytes);
	    
	    //write output to the buffer
	    memcpy(writebuffer + mypos, dstbuf, outnbytes);
	}

	MPI_CHECK(MPI_Barrier(core.comm));

	//collectively write this to the file
	{
	    const size_t nbytes = *bytes_tail;
	    assert(nbytes < (1ull << 31));

	    int s,e;
	    nothreads::divide_workload(wid, nw, nbytes, s, e);

	    MPI_Status status;
	    MPI_CHECK(MPI_File_write_at_all(fout, allnbytes + s, writebuffer + s, e - s, MPI_BYTE, &status));

	    allnbytes += nbytes;
	}
    }

    MPI_CHECK(MPI_File_set_size(fout, allnbytes));
    MPI_CHECK(MPI_File_close(&fout));
    
    MPI_CHECK(MPI_Barrier(node.comm));

    const double t1 = MPI_Wtime();

    const size_t overallfp = ntiles * (size_t)tilesize * sizeof(int16_t);

    if (verbose)
     	printf("%.3fs, %zd bytes, %.3f GB/s\n", t1 - t0, overallfp, overallfp / 1e9 / (t1 - t0));

    _mm_free(dstbuf);
    _mm_free(srcbuf);
    
    nothreads::shmem_free(metabuffer);
    nothreads::shmem_free(writebuffer);
    nothreads::shmem_free(readbuffer);
    
    MPI_CHECK(MPI_Finalize());

    return 0;
}
