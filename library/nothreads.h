#pragma once

#include <mpi.h>

#include "nt.h"

namespace nothreads
{
    typedef nt_layer layer;

    extern layer node, numa, core, hwt;

    void init();

    void * shmem_alloc(layer, const size_t nbytes);

    void shmem_free(const void *);

    void batch();

    void divide_workload(layer, const int workload, int& start, int& end);

    void divide_workload(const int rank, const int nranks, const int workload, int& start, int& end);

    int fetch_and_add(int * counter, int value);
}
