#include <cassert>
#include <cstddef>
#include <set>
#include <map>
#include <algorithm>

#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>

#if defined(__linux__) || defined(__APPLE__)
#ifdef _USE_XPMEM_

extern "C"
{
#include <xpmem.h>
}
#else
#define _USE_SYSV_
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/mman.h>
#endif
#else
#include <windows.h>
#endif

#include "nothreads.h"
#include "nothreads-utils.h"

namespace nothreads
{
    extern bool initialized;

    std::set<int> available;

#ifdef _USE_XPMEM_
    struct shmem_item
    {
	MPI_Comm comm;
	int mid;
	xpmem_segid_t segid;
	xpmem_apid_t apid;
    };
#elif defined(_USE_WIN_)
    struct shmem_item
    {
	int mid;
	MPI_Win win;
	MPI_Comm comm;
    };
#elif defined( _USE_SYSV_)
    struct shmem_item
    {
	enum { NMAX = 128 };

	int mid, nseg, segid[NMAX];
	void * buf[NMAX];
	size_t nbytes;
	MPI_Comm comm;
    };
#else
    struct shmem_item { int mid; size_t nbytes; MPI_Comm comm;};
#endif

    std::map<void *, shmem_item> taken;
#ifdef _WIN32
    std::map<void *, HANDLE>  ptr2h;
#endif

    bool shmeminit = false;

    void shmem_init()
    {
	for(int i = 0; i < 4096; ++i)
	    available.insert(i);
    }

    uint32_t hash_name (
	const char * name )
    {
        //compute hash
        unsigned char * str = (unsigned char *)name;
        unsigned int hash = 5381;
        int32_t c;

        while ((c = *str++))
            hash = ((hash << 5) + hash) + c;

	return hash;
    }

    void form_name (
	MPI_Comm comm,
	const int mid,
	char desired_name[2048])
    {
	char namecomm[MPI_MAX_OBJECT_NAME];
	int len;
	MPI_CHECK(MPI_Comm_get_name(comm, namecomm, &len));
	namecomm[len] = 0;
        const int32_t hname = hash_name(namecomm) & 0x7fffffff;

	sprintf(desired_name, "%d_shmem%03d", hname, mid);
    }

    __attribute__ ((visibility ("default")))
    void * shmem_alloc(layer layer, const size_t nbytes)
    {
	if (!shmeminit)
	{
	    shmem_init();

	    shmeminit = true;
	}

	bool verbose = false;

	if (getenv("NOTHREADS_VERBOSE"))
	    verbose = (bool)atoi(getenv("NOTHREADS_VERBOSE"));

	assert(initialized);
	assert(available.size());

	const int mid = *available.begin();

	available.erase(available.begin());

	void * ptr;

	char desired_name[2048];

        form_name(layer.comm, mid, desired_name);

	int lr;
	MPI_CHECK(MPI_Comm_rank(layer.comm, &lr));

#ifdef _USE_XPMEM_

	const int master = lr == 0;
	const long pgsz = sysconf(_SC_PAGESIZE);

	shmem_item item;
	item.mid = mid;
	item.comm = layer.comm;
	item.segid = -1;
	item.apid = -1;
	ptr = NULL;

	if (master && verbose)
	    fprintf(stderr,
		    "XPMEM implementation version: %x\nsizeof(segid): %zd\n",
		    xpmem_version(), sizeof(item.segid));

	if (master)
	{
	    SYS_CHECK(0 == posix_memalign(&ptr, pgsz, nbytes),
		      "error in posix_memalign");

	    /* touch pages */
	    for(size_t i = 0; i < nbytes; i += pgsz)
		*(i + (char *)ptr)= 0;

	    SYS_CHECK(-1 !=
		  (item.segid = xpmem_make
		   (ptr, nbytes, XPMEM_PERMIT_MODE, (void *)0666)),
		  "xpmem_make failed");

	    SYS_CHECK(ptr != NULL, "error data is NULL");
	}

	MPI_CHECK( MPI_Bcast(&item.segid, 1, MPI_INT64_T, 0, layer.comm));

	if (lr)
	{
	    SYS_CHECK(-1 !=
		      (item.apid = xpmem_get(item.segid, XPMEM_RDWR, XPMEM_PERMIT_MODE, NULL)),
		      "xpmem_get error");

	    struct xpmem_addr addr;
	    addr.apid = item.apid;
	    addr.offset = 0;

	    SYS_CHECK(NULL !=
		      (ptr = xpmem_attach(addr, nbytes, NULL)),
		      "error xpmem_attach");
	}

	MPI_CHECK(MPI_Barrier(layer.comm));

#elif defined(_USE_WIN_)
	shmem_item item;
	item.mid = mid;
	item.comm = layer.comm;
	ptr = NULL;
	MPI_Win win;

	if (verbose)
	    if (lr == 0)
		fprintf(stderr, "MPI_Win_allocate_shared implementation\n");

	MPI_CECK(MPI_Win_allocate_shared (nbytes, 1, MPI_INFO_NULL, //MPI_SAME_SIZE | MPI_SAME_DISP_UNIT,
					   layer.comm, &ptr, &win));

	item.win = win;
#elif defined(_USE_SYSV_)

	if (verbose)
	    if (lr == 0)
		fprintf(stderr, "SysV implementation\n");

	shmem_item item;

	{
	    memset(&item, 0, sizeof(item));

	    item.mid = mid;
	    item.comm = layer.comm;

	    const size_t segment_max_nbytes = std::min(nbytes, (size_t)(4ull << 30ull));

	    const int nseg = (int)((nbytes + segment_max_nbytes - 1) / segment_max_nbytes);
	    item.nseg = nseg;

	    void * reserved = NULL;

	    if (nseg > 1)
	    {
		const size_t pgsz = sysconf(_SC_PAGESIZE);

		if (verbose)
		    if (lr == 0)
			fprintf(stderr, "pagesize is %zd\n", (ptrdiff_t)pgsz);

		SYS_CHECK(0 == posix_memalign((void **)&reserved, pgsz, (size_t)nseg * (size_t)(4ull << 30ull)), "posix_memalign");
		void * t = reserved;
		free(t);
	    }

	    MPI_CHECK(MPI_Barrier(layer.comm));

	    char commname[MPI_MAX_OBJECT_NAME];

	    /* read commname */
	    {
		int len;
		MPI_CHECK(MPI_Comm_get_name(layer.comm, commname, &len));
		commname[len] = 0;
	    }

	    for(int seg = 0; seg < nseg; ++seg)
	    {
		const size_t segment_nbytes = std::min(nbytes - (size_t)seg * segment_max_nbytes, segment_max_nbytes);

		char segname[2048];
		sprintf(segname, "/tmp/%s.%zd.%d.%d",
			commname, (ptrdiff_t)nbytes, mid, seg);

		key_t k = hash_name(segname);

		SYS_CHECK(-1 != (item.segid[seg] =
				 shmget(k, segment_nbytes, SHM_NORESERVE | IPC_CREAT | S_IRUSR | S_IWUSR)), "shmget");

		void * const target = (char *)reserved + segment_max_nbytes * (size_t)seg;

		void * tmp;
		SYS_CHECK(MAP_FAILED != (tmp = shmat(item.segid[seg], target, 0)), "shmat");

		if (reserved)
		    assert(tmp == target);

		item.buf[seg] = tmp;
	    }

	    ptr = item.buf[0];

	    MPI_CHECK(MPI_Barrier(layer.comm));
	}
#elif defined(__linux__)
/*	int fd;
	SYS_CHECK(-1 != (fd = shm_open(desired_name, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR)), "creating output file");
	SYS_CHECK(0 == ftruncate(fd, nbytes), "truncate out file");
	SYS_CHECK(MAP_FAILED != (ptr = mmap(NULL, nbytes, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0)), "memory map");

	shmem_item item = { mid, nbytes, layer.comm };
#elif defined(__APPLE__)
shm_unlink(desired_name);*/
	errno = 0;

	if (verbose)
	    if (lr == 0)
		fprintf(stderr, "POSIX implementation\n");

	int rank;
        MPI_CHECK(MPI_Comm_rank(layer.comm, &rank));

        int fd = -1;

        if (rank == 0)
	    SYS_CHECK(-1 != (fd = shm_open(desired_name, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR)),
		"shm_open master");

        MPI_CHECK(MPI_Barrier(layer.comm));

        if (rank > 0)
            SYS_CHECK(-1 != (fd = shm_open(desired_name, O_RDWR| O_CREAT, S_IRUSR | S_IWUSR)),
		      "shm_open slaves");

        MPI_CHECK(MPI_Barrier(layer.comm));

	if (rank == 0)
	{
	    struct stat mapstat;

            if (-1 != fstat(fd, &mapstat) && mapstat.st_size != (ptrdiff_t)nbytes)
                SYS_CHECK(0 == ftruncate(fd, nbytes), "ftruncate");
	}

        ptr = mmap(NULL, nbytes, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);
        SYS_CHECK(ptr != MAP_FAILED, "memory map");

	shmem_item item = { mid, nbytes, layer.comm };
#else
	HANDLE fd;

	int rank;
        MPI_CHECK(MPI_Comm_rank(layer.comm, &rank));

	MPI_CHECK(MPI_Barrier(layer.comm));

	if (rank == 0)
	    WIN_CHECK(fd = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, nbytes, desired_name), "Create file mapping");

	MPI_CHECK(MPI_Barrier(layer.comm));

	if (rank)
	    WIN_CHECK(fd = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, desired_name), "Open file mapping");

	MPI_CHECK(MPI_Barrier(layer.comm));
		
	WIN_CHECK(ptr = MapViewOfFile(fd, FILE_MAP_ALL_ACCESS, 0, 0, nbytes), "Map view of file roi");
	ptr2h[ptr] = fd;

	shmem_item item = { mid, nbytes, layer.comm };
#endif
	//printf("picked: %d, %zd bytes, name: %s\n", mid, nbytes, desired_name);
	taken[ptr] = item;

	return ptr;
    }

     __attribute__ ((visibility ("default")))
    void shmem_free(const void * ptr)
    {
	assert(shmeminit);

	std::map<void *, shmem_item>::iterator it = taken.find((void *)ptr);
	assert(it != taken.end());

	const shmem_item item = it->second;
	taken.erase(it);

	MPI_CHECK(MPI_Barrier(item.comm));
#ifdef _USE_XPMEM_
	int lr;
	MPI_CHECK(MPI_Comm_rank(item.comm, &lr));

	if (lr)
	{
	    SYS_CHECK(-1 != xpmem_detach((void *)ptr), "xpmem_detach error");
	    SYS_CHECK(-1 != xpmem_release(item.apid), "xpmem_release error");
	}
	else
	    SYS_CHECK(-1 != xpmem_remove(item.segid), "xpmem_remove error");

	MPI_CHECK(MPI_Barrier(item.comm));
#elif defined(_USE_WIN_)
	MPI_Win win = item.win;
	MPI_CHECK(MPI_Win_free(&win));
#elif defined(_USE_SYSV_)

	for(int seg = item.nseg - 1; seg >= 0; --seg)
	{
	    SYS_CHECK(0 == shmctl(item.segid[seg], IPC_RMID, NULL), "shmctl");
	    SYS_CHECK(0 == shmdt(item.buf[seg]), "shmdt");
	}

	MPI_CHECK(MPI_Barrier(item.comm));

#elif defined(__linux__) || defined(__APPLE__)
	SYS_CHECK(0 == munmap((void *)ptr, item.nbytes), "munmap");

	int rank;
	MPI_CHECK(MPI_Comm_rank(item.comm, &rank));

	if (rank == 0)
	{
	    char desired_name[2048];
	    form_name(item.comm, item.mid, desired_name);

	    SYS_CHECK(0 == shm_unlink(desired_name), "shm_unlink");
	}

	MPI_CHECK(MPI_Barrier(item.comm));
#else
	UnmapViewOfFile(ptr);
	CloseHandle(ptr2h[(void *)ptr]);
	ptr2h.erase((void *)ptr);
#endif

	available.insert(item.mid);
    }
}
