#pragma once

#include <cstdio>
#include <cstdlib>
#include <cerrno>
#include <cstring>

#define SYS_CHECK(stmt, ...)						\
    do									\
    {									\
	if (!(stmt))							\
	{								\
	    fprintf(stderr,						\
		    "OS ERROR MESSAGE: %s\n", strerror(errno));		\
	    								\
	    fprintf(stderr,						\
		    "ERROR: "						\
		    #stmt);						\
									\
	    fprintf(stderr, "\n" # __VA_ARGS__				\
		    " in file* %s at line %d\n"				\
		    "\nexiting now.\n",					\
		    __FILE__, __LINE__);				\
									\
	    fflush(stderr);						\
									\
	    exit(EXIT_FAILURE);						\
	}								\
    }									\
    while(0)


#include <mpi.h>

#ifndef MPI_CHECK
#define MPI_CHECK(stmt)							\
    do									\
    {									\
    const int code = stmt;						\
    									\
    if (code != MPI_SUCCESS)						\
    {									\
	char error_string[2048];					\
	int length_of_error_string = sizeof(error_string);		\
	MPI_Error_string(code, error_string, &length_of_error_string);	\
									\
	fprintf(stderr,							\
		"ERROR!\n" #stmt " mpiAssert: %s %d %s\n",		\
		__FILE__, __LINE__, error_string);			\
	fflush(stderr);							\
									\
	MPI_Abort(MPI_COMM_WORLD, code);				\
    }									\
    }									\
    while(0)

#endif

#ifdef _WIN32
#include <string>
#include <windows.h>

//Returns the last Win32 error, in string format. Returns an empty string if there is no error.
inline std::string GetLastErrorAsString()
{
    //Get the error message, if any.
    DWORD errorMessageID = ::GetLastError();
    if(errorMessageID == 0)
        return std::string(); //No error message has been recorded

    LPSTR messageBuffer = nullptr;
    size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                                 NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

    std::string message(messageBuffer, size);

    //Free the buffer.
    LocalFree(messageBuffer);

    return message;
}

#define WIN_CHECK(stmt, str)					\
    do								\
    {								\
	if (!(stmt))						\
	{							\
	    fprintf(stderr,					\
		    "ERROR %d: %s\n"				\
		    "with %s in file %s at line %d\n"		\
		    "exiting now.\n",				\
		    GetLastError(),				\
		    GetLastErrorAsString().c_str(),		\
		    str, __FILE__, __LINE__);			\
								\
	    exit(EXIT_FAILURE);					\
	}							\
    }								\
    while(0)

#endif //_WIN32
