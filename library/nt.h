#pragma once

#include <stddef.h>
#include <mpi.h>

typedef struct
{
    MPI_Comm comm;
    int count, id;
    int global_count, global_id;
} nt_layer;

extern nt_layer nt_node, nt_numa, nt_core, nt_hwt;

#ifdef __cplusplus
extern "C"
{
#endif
    void nt_init();

    void * nt_shmem (
	nt_layer layer,
	const size_t nbytes);

    void nt_free(const void *);

    void nt_batch();

    void nt_workload (
	nt_layer layer,
	const int workload,
	int * const __restrict__ start,
	int * const __restrict__ end);

    void nt_workload_global (
	nt_layer layer,
	const int workload,
	int * const __restrict__ start,
	int * const __restrict__ end);

    void nt_finalize();

#ifdef __cplusplus
}
#endif
