#include "nt.h"

#include "nothreads.h"

__attribute__ ((visibility ("default"))) 
nt_layer nt_node, nt_numa, nt_core, nt_hwt;

extern "C"
__attribute__ ((visibility ("default"))) 
void nt_init()
{
    nothreads::init();

    nt_node = nothreads::node;
    nt_numa = nothreads::numa;
    nt_core = nothreads::core;
    nt_hwt = nothreads::hwt;
}

extern "C"
__attribute__ ((visibility ("default")))
void * nt_shmem (
    nt_layer layer,
    const size_t nbytes)
{
    return nothreads::shmem_alloc(layer, nbytes);
}

extern "C"
__attribute__ ((visibility ("default"))) 
void nt_free(const void * p)
{
    nothreads::shmem_free(p);
}

extern "C"
__attribute__ ((visibility ("default"))) 
void nt_batch()
{
    nothreads::batch();
}

extern "C"
__attribute__ ((visibility ("default"))) 
void nt_workload (
    nt_layer layer,
    const int workload,
    int * const __restrict__ start,
    int * const __restrict__ end)
{
    return nothreads::divide_workload(layer, workload, *start, *end);
}

extern "C"
__attribute__ ((visibility ("default"))) 
void nt_workload_global (
    nt_layer layer,
    const int workload,
    int * const __restrict__ start,
    int * const __restrict__ end)
{
    return nothreads::divide_workload(layer.global_id, layer.global_count, workload, *start, *end);
}

extern "C"
__attribute__ ((visibility ("default"))) 
void nt_finalize ()
{
    MPI_Comm_free(&nt_hwt.comm);
    MPI_Comm_free(&nt_core.comm);
    MPI_Comm_free(&nt_numa.comm);
    MPI_Comm_free(&nt_node.comm);
}
