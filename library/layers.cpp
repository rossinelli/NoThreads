#include <cassert>
#include <stdint.h>

#include <algorithm>

#include <sched.h>

#ifdef __linux__
#include <numa.h>
#ifdef _SEM_
#include <semaphore.h>
#include <fcntl.h>
#endif
#elif defined(__APPLE__)
#include <cpuid.h>

#define CPUID(INFO, LEAF, SUBLEAF)					\
    __cpuid_count(LEAF, SUBLEAF, INFO[0], INFO[1], INFO[2], INFO[3])

#define GETCPU(CPU) {					\
	uint32_t CPUInfo[4];				\
	CPUID(CPUInfo, 1, 0);				\
	/* CPUInfo[1] is EBX, bits 24-31 are APIC ID */ \
	if ( (CPUInfo[3] & (1 << 9)) == 0) {		\
	    CPU = -1;  /* no APIC on chip */		\
	}						\
	else {						\
	    CPU = (unsigned)CPUInfo[1] >> 24;		\
	}						\
	if (CPU < 0) CPU = 0;				\
    }
#else
#include <windows.h>
#endif

#include <unistd.h>

#include "nothreads-utils.h"
#include "nothreads.h"

namespace nothreads
{
    bool initialized = false;

    __attribute__ ((visibility ("default"))) layer node, numa, core, hwt;

#ifdef _SEM_
    sem_t * sem;
#endif

    int32_t hash_node()
    {
	int nodenamelen;
	char nodename[2048];
	MPI_CHECK(MPI_Get_processor_name(nodename, &nodenamelen));

	//compute hash
	unsigned char * str = (unsigned char *)nodename;
	unsigned int hash = 5381;
	int32_t c;

	while ((c = *str++))
	    hash = ((hash << 5) + hash) + c;

	//msb shave
	hash = (uint32_t)(hash & ~((int64_t)1 << (int64_t)31));

	return hash;
    }

    void print_comm_name(MPI_Comm comm)
    {
	char comm_name[MPI_MAX_OBJECT_NAME];
	int len;
	MPI_CHECK(MPI_Comm_get_name(comm, comm_name, &len));
	comm_name[len] = 0;

	MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));

	printf("COMM NAME: %s %zd %d\n", comm_name, sizeof(comm_name), len);
	fflush(stdout);

	MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));
    }

    bool uniform_value(MPI_Comm comm, int val)
    {
	int nranks;
	MPI_CHECK(MPI_Comm_size(comm, &nranks));

	int all[nranks];
	MPI_CHECK(MPI_Allgather(&val, 1, MPI_INT, all, 1, MPI_INT, comm));

	for(int i = 0; i < nranks; ++i)
	    if (val != all[i])
		return false;

	return true;
    }

    void split_comm(
	MPI_Comm parentcomm,
	MPI_Comm& comm,
	const int color,
	const int key,
	const char name[],
	int& ncomms,
	int& commid)
    {
	int rank, nranks;
	MPI_CHECK(MPI_Comm_rank(parentcomm, &rank));
	MPI_CHECK(MPI_Comm_size(parentcomm, &nranks));

	MPI_CHECK(MPI_Comm_split(parentcomm, color, key, &comm));

	{
	    int * all = new int[nranks];

	    MPI_CHECK(MPI_Allgather(&color, 1, MPI_INTEGER, all, 1, MPI_INTEGER, parentcomm));

	    std::sort(all, all + nranks);

	    ncomms = std::unique(all, all + nranks) - all;
	    assert(ncomms <= nranks);

	    commid = std::lower_bound(all, all + ncomms, color) - all;
	    assert(commid < ncomms);

	    delete [] all;
	}

	char fullname[MPI_MAX_OBJECT_NAME];
	{
	    char parentname[MPI_MAX_OBJECT_NAME];
	    int len;
	    MPI_CHECK(MPI_Comm_get_name(parentcomm, parentname, &len));
	    parentname[len] = 0;

	    if (ncomms > 1)
		sprintf(fullname, "%s_%s%x", parentname, name, color);
	    else
		sprintf(fullname, "%s_%s", parentname, name);

	    MPI_CHECK(MPI_Comm_set_name(comm, fullname));
	}
    }

    int pin_to_hwt(MPI_Comm nodecomm)
    {
	bool nopinning = false;

	if (getenv("NOTHREADS_NOPINNING"))
	    nopinning = (bool)atoi(getenv("NOTHREADS_NOPINNING"));

	int hwtid;

	{

#ifdef __linux__
	    hwtid = sched_getcpu();
#elif defined(__APPLE__)
	    GETCPU(hwtid);
#else
	    hwtid = GetCurrentProcessorNumber();
#endif

       	    int nranks, rank;
	    MPI_CHECK(MPI_Comm_size(nodecomm, &nranks));
	    MPI_CHECK(MPI_Comm_rank(nodecomm, &rank));

	    int alids[nranks];
	    MPI_CHECK(MPI_Allgather(&hwtid, 1, MPI_INT, alids, 1, MPI_INT, nodecomm));

	    int unique = 1;
	    for(int i = 0; i < nranks; ++i)
		unique = unique && (alids[i] != hwtid || i == rank);

	    MPI_CHECK(MPI_Allreduce(MPI_IN_PLACE, &unique, 1, MPI_INT, MPI_MIN, nodecomm));

	    if (!unique)
	    {
		if (rank == 0)
		{
		    if (!nopinning)
			fprintf(stderr,
				"hwtid overwritten! ");

		    for(int i = 0; i < nranks; ++i)
			fprintf(stderr, "%d ", alids[i]);

		    fprintf(stderr, "\n");

		    if (nopinning)
		    {
			fprintf(stderr,
				"ERROR: mapping process:coreid not unique.\nABORTING NOW.\n");

			__builtin_trap();
		    }

		    fflush(stderr);
		}

		hwtid = rank;
	    }
	}

#ifdef __linux__
	cpu_set_t mask;
	CPU_ZERO(&mask);
	CPU_SET(hwtid, &mask);

	const int nhwts = sysconf(_SC_NPROCESSORS_ONLN);

	if (!nopinning)
	    SYS_CHECK(0 == sched_setaffinity(getpid(), nhwts, &mask), "setting affinity");
#elif defined(_WIN32)
	uint64_t mask = 0;
	assert(hwtid < 64 && hwtid >= 0);
	mask |= (uint64_t)1 << hwtid;

	if (!nopinning)
	    SYS_CHECK(0 != SetProcessAffinityMask(GetCurrentProcess(), mask), "set affinity mask");
#endif

	return hwtid;
    }

    __attribute__ ((visibility ("default")))
    void init()
    {
	int rank, nranks;
	MPI_CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank));
	MPI_CHECK(MPI_Comm_size(MPI_COMM_WORLD, &nranks));
#if 0
	const int nodehash = hash_node() + rank;
#else
	const int nodehash = hash_node(); // + rank;
#endif

	{
	    int nc = 0, nid = 0;

	    split_comm(MPI_COMM_WORLD, node.comm,
		       0, nodehash, "", nc, nid);

	    assert(nc == 1);
	    assert(nid < nc);
	}

	split_comm(node.comm, numa.comm,
		   nodehash, 0, "nodecomm", node.count, node.id);

	node.global_count = node.count;

	const int hwtid = pin_to_hwt(numa.comm);
#ifdef __linux__
	int numaid = numa_node_of_cpu(hwtid);
#elif defined(__APPLE__)
	int numaid = 0;
#else
	int numaid;
	{
	    UCHAR result;
	    SYS_CHECK(0 != GetNumaProcessorNode(hwtid, &result), "get numa node");
	    numaid = result;
	}
#endif

#ifdef _SEM_
	{
	    char name[1024];
	    sprintf(name, "nothreads_numa%d_semaphore", numaid);
	    SYS_CHECK(SEM_FAILED != (sem = sem_open(name, O_CREAT, S_IRUSR | S_IWUSR, 1)), "creating semaphore");
	}
#endif

	//bool nonuma = false;

	if (getenv("NOTHREADS_NONUMA"))
	    if (atoi(getenv("NOTHREADS_NONUMA")) == 1)
	    {
		//nonuma = true;
		numaid = 0;
	    }

	int coreid;

#ifdef __linux__
	{
	    char fname[2048];
	    sprintf(fname, "/sys/devices/system/cpu/cpu%d/topology/thread_siblings_list", hwtid);
	    FILE * f = fopen(fname, "r");

	    int s[8];
	    int __attribute__((unused)) n;

	    n = fscanf(f, "%d,%d,%d,%d,%d,%d,%d,%d",
		       s + 0, s + 1, s + 2, s + 3,
		       s + 4, s + 5, s + 6, s + 7);

	    assert(n);

	    coreid = s[0];

	    fclose(f);
	}
#elif defined(__APPLE__)
        coreid = hwtid;
#else
	{
	    DWORD len = 0;
	    GetLogicalProcessorInformation(NULL, &len);

	    const int n = len / sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
	    SYSTEM_LOGICAL_PROCESSOR_INFORMATION buf [n];
	    assert(len %  sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) == 0);

	    SYS_CHECK(0 != GetLogicalProcessorInformation(buf, &len), "get processor info");

	    bool found = false;
	    int corecount = 0;
	    for(int i = 0; i < n; ++i)
		if (buf[i].Relationship == RelationProcessorCore)
		{
		    for(int j = 0; j < 64; ++j)
			if (buf[i].ProcessorMask & ((uint64_t)1 << j))
			    if (j == hwtid)
			    {
				coreid = corecount;
				found = true;
				goto corefound;
			    }

		    ++corecount;
		}
	corefound:
	    assert(found);

	}
#endif

	split_comm(numa.comm, core.comm,
		   numaid, coreid, "numa", numa.count, numa.id);

	//compute numa.global_count
	{
	    int rank;
	    MPI_CHECK(MPI_Comm_rank(numa.comm, &rank));

	    int count = 0;
	    if (rank == 0)
		count = numa.count;

	    MPI_CHECK(MPI_Allreduce(&count, &numa.global_count, 1, MPI_INT, MPI_SUM, node.comm));
	}

	split_comm(core.comm, hwt.comm,
		   coreid, hwtid, "core", core.count, core.id);

	//compute core.global_count
	{
	    int rank;
	    MPI_CHECK(MPI_Comm_rank(core.comm, &rank));

	    int count = 0;
	    if (rank == 0)
		count = core.count;

	    MPI_CHECK(MPI_Allreduce(&count, &core.global_count, 1, MPI_INT, MPI_SUM, node.comm));
	}

	{
	    int nranks, rank;
	    MPI_CHECK(MPI_Comm_size(hwt.comm, &nranks));
	    MPI_CHECK(MPI_Comm_rank(hwt.comm, &rank));

	    hwt.id = rank;
	    hwt.count = nranks;
	}

	hwt.global_count = nranks;

	//recreate the node.comm, numa.comm, core.comm by having a better ordering of the ranks
	{
	    int maxnumas, maxcores, maxhwts;
	    MPI_CHECK(MPI_Allreduce(&numa.count, &maxnumas, 1, MPI_INT, MPI_MAX, node.comm));
	    MPI_CHECK(MPI_Allreduce(&core.count, &maxcores, 1, MPI_INT, MPI_MAX, node.comm));
	    MPI_CHECK(MPI_Allreduce(&hwt.count, &maxhwts, 1, MPI_INT, MPI_MAX, node.comm));

	    MPI_CHECK(MPI_Comm_free(&node.comm));

	    int __attribute__((unused)) tmp1, tmp2;
	    split_comm(MPI_COMM_WORLD, node.comm,
		       0, hwt.id + maxhwts * (core.id + maxcores * (numa.id + maxnumas * node.id)), "", tmp1, tmp2);

	    MPI_CHECK(MPI_Comm_free(&numa.comm));

	    split_comm(node.comm, numa.comm,
		       nodehash, hwt.id + maxhwts * (core.id + maxcores * numa.id), "numacomm", node.count, node.id);

	    MPI_CHECK(MPI_Comm_free(&core.comm));

	    split_comm(numa.comm, core.comm,
		       numaid, hwt.id + maxhwts * core.id, "corecomm", numa.count, numa.id);
	}

	//compute global ids
	{
	    node.global_id = node.id;

	    const int numa_master = core.id == 0 && hwt.id == 0;
	    numa.global_id = 0;
	    MPI_CHECK(MPI_Scan(&numa_master, &numa.global_id, 1, MPI_INT, MPI_SUM, node.comm));
	    numa.global_id -= 1;

	    assert(uniform_value(core.comm, numa.global_id));

	    const int core_master = hwt.id == 0;
	    core.global_id = 0;
	    MPI_CHECK(MPI_Scan(&core_master, &core.global_id, 1, MPI_INT, MPI_SUM, node.comm));
	    core.global_id -= 1;
	    assert(uniform_value(hwt.comm, core.global_id));

	    const int one = 1;
	    hwt.global_id = 0;
	    MPI_CHECK(MPI_Scan(&one, &hwt.global_id, 1, MPI_INT, MPI_SUM, node.comm));
	    hwt.global_id -= 1;
	}

	if (rank == 0)
	    printf("we're using %d numa nodes and %d cores overall\n",
		   numa.global_count, core.global_count);

	fflush(stdout);

	MPI_CHECK(MPI_Barrier(node.comm));

	//print_comm_name(hwt.comm);

	initialized = true;
    }

    __attribute__ ((visibility ("default")))
    void batch()
    {
#ifdef __linux__
	int policy;
	SYS_CHECK(-1 != (policy = sched_getscheduler(0)), "get scheduler");

	sched_param param;
	param.sched_priority = 0;
	SYS_CHECK(-1 != sched_setscheduler(0, SCHED_BATCH, &param), "set sched");

	SYS_CHECK(-1 != (policy = sched_getscheduler(0)), "get scheduler");
	assert(SCHED_BATCH == policy);
#elif defined(_WIN32)
	HANDLE tid = GetCurrentThread();
	SYS_CHECK(0 != SetThreadPriority(tid, THREAD_PRIORITY_ABOVE_NORMAL), "priority");
	assert(THREAD_PRIORITY_ABOVE_NORMAL == GetThreadPriority(GetCurrentThread()));
#endif
    }

    __attribute__ ((visibility ("default")))
    void divide_workload(
	const int rank,
	const int nranks,
	const int ntasks,
	int& worker_start,
	int& worker_end)
    {
	const int worker = rank;
	const int nworkers = nranks;

	const int share = ntasks / nworkers;
	const int rmnd = ntasks % nworkers;

	int workloads[nworkers];

	for(int i = 0; i < nworkers; ++i)
	    workloads[i] = share + (int)(i < rmnd);

	int start[nworkers + 1];

	start[0] = 0;

	for(int i = 1; i <= nworkers; ++i)
	    start[i] = start[i - 1] + workloads[i - 1];

	assert(start[nworkers] == ntasks);

	worker_start = start[worker];
	worker_end = start[worker + 1];
    }

    __attribute__ ((visibility ("default")))
    void divide_workload(
	layer layer,
	const int ntasks,
	int& worker_start,
	int& worker_end)
    {
	return divide_workload(layer.id, layer.count, ntasks, worker_start, worker_end);
    }

    __attribute__ ((visibility ("default")))
    int fetch_and_add( int * mem, int val )
    {
#ifdef _SEM_

	SYS_CHECK(0 == sem_wait(sem), "sem wait");

	const int retval = *mem;
	*mem += val;

	SYS_CHECK(0 == sem_post(sem), "sem post");

	return retval;

#else

	asm volatile("lock; xaddl %%eax, %2;"
		     :"=a" (val)
		     :"a" (val), "m" (*mem)
		     :"memory");
	return val;

#endif
    }
}
