#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <stdint.h>

#include <fcntl.h>
#include <unistd.h>

#ifdef __linux__
#include <sys/mman.h>
#else
#include <windows.h>
#include "win-utils.h"
#endif

#include "mpi-utils.h"
#include "std-utils.h"

int main(int argc, char ** argv)
{
    MPI_CHECK(MPI_Init(&argc, &argv));

    const double tstart = MPI_Wtime();
    
    MPI_Comm comm = MPI_COMM_WORLD;

    int rank, nranks;
    MPI_CHECK(MPI_Comm_rank(comm, &rank));
    MPI_CHECK(MPI_Comm_size(comm, &nranks));

    const int xsize = 1920, ysize = 1920, zsize = 64 * 64;
    if (rank == 0)
	printf("size: %d %d %d\n", xsize, ysize, zsize);
    const int slicesize = xsize * ysize;

    const char * outpath = "test.raw";
    const size_t maxbytes = sizeof(int16_t) * xsize * ysize * (size_t)zsize * 1.5;
    void * dstbuf;
#ifdef __linux__

    const int fd = open(outpath, O_TRUNC | O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    SYS_CHECK(fd != -1, "creating output file");

    const size_t dstsize = (size_t)ceil(maxMB * 1024. * 1024.);
    SYS_CHECK(-1 != ftruncate(fd, dstsize), "truncate out file");

    dstbuf = mmap(NULL, dstsize, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);
    SYS_CHECK(dstbuf != MAP_FAILED, "memory map");

#else

    HANDLE fd = INVALID_HANDLE_VALUE, filemap = INVALID_HANDLE_VALUE;
    const char * namemapping = "Local\\myfilemapping";
	union HiLo
	{
	    size_t val;
	    uint32_t words[2];
	} ;
    if (rank == 0)
    {
	fflush(stdout);

	fd = CreateFile(outpath,
			GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	WIN_CHECK(fd != INVALID_HANDLE_VALUE, "Create file");

	/*const uint32_t sizehi = maxbytes >> 32;
	  const uint32_t sizelo = maxbytes & (size_t)0xffffffff;*/
	HiLo maxsize;

	maxsize.val = maxbytes;

	WIN_CHECK( INVALID_HANDLE_VALUE != (filemap = CreateFileMapping(fd, NULL, PAGE_READWRITE, maxsize.words[1], maxsize.words[0], namemapping)),
		  "Creating file mapping");
    }

    MPI_CHECK(MPI_Barrier(comm));

    if (rank != 0)
	WIN_CHECK(NULL != (filemap = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, namemapping)), "Open file mapping roi");

   

    MEMORY_BASIC_INFORMATION info[32];
    size_t actualsize = VirtualQuery(dstbuf, info, sizeof(info));
    printf("actualsize: %d sizeof 1 %d\n", actualsize, sizeof(info[1]));

    printf("region size: %zd -> %.3f GB\n",
	   info[0].RegionSize, info[0].RegionSize / double(1<<30));
    SYSTEM_INFO sysinfo;
    GetSystemInfo(&sysinfo);
    const size_t pagesize =  sysinfo.dwAllocationGranularity;
    printf("pagesize: %d\n", pagesize);

#endif
    fflush(stdout);
    MPI_CHECK(MPI_Barrier(comm));

//return 0;

    const size_t myviewsize = 2ull << 30;
    const int nslices = myviewsize / (sizeof(int16_t) * xsize * ysize);
    printf("nslices: %d\n", nslices);
    //return 0;
    

    for(int zbase = 0; zbase < zsize; zbase += nslices)
    {

	const size_t _base = sizeof(int16_t) * xsize * ysize * (size_t)zbase;
	const size_t base = _base & ~(pagesize - 1);

	HiLo val;
	val.val = base;

	printf("zbase: %d of %d ~ 0x%x 0x%x ~ %zd\n", zbase, nslices, val.words[1], val.words[0], val.val % 4096);
	fflush(stdout);
	void * tmp ; 
	WIN_CHECK(NULL != (tmp = (char *)MapViewOfFile(filemap, FILE_MAP_ALL_ACCESS, val.words[1], val.words[0], myviewsize)), "Map view of file");

	void* dstbuf = (uint8_t * )tmp + _base - base;
	int16_t * dstbuf16 = (int16_t *)dstbuf;
	
	for(int i = rank; i < nslices; i += nranks)
	{
	    /*if (rank == 0)
		printf("%d \n", i);
	    
	    fflush(stdout);
	    */
	    memset(dstbuf16 + xsize * ysize * (size_t)i,
		   rank,
		   sizeof(*dstbuf16) * xsize * ysize);
	}
	//WIN_CHECK(FlushViewOfFile(dstbuf16, sizeof(int16_t) * nslices * (size_t)xsize *ysize), "flush" );


	WIN_CHECK(UnmapViewOfFile(tmp), "unmap");
    }

    MPI_CHECK(MPI_Barrier(comm));
    
    /*
    int xsizedst, ysizedst, zsizedst;

    const int retval = roi(inpath,
			   s[0], s[1], s[2], s[0] + ex[0], s[1] + ex[1], s[2] + ex[2],
			   true, qlayers, maxbytes, (short *)dstbuf,
			   &xsizedst, &ysizedst, &zsizedst);
    */

    const size_t finalsize = sizeof(int16_t) * xsize * ysize * (size_t)zsize;
    
#ifdef __linux__
    SYS_CHECK(-1 != munmap(dstbuf, dstsize), "unmapping");

    SYS_CHECK(-1 != ftruncate(fd,
			      finalsize), "retruncate out file");

    SYS_CHECK(0 == close(fd), "closing output file");
#else
    //WIN_CHECK(0 != UnmapViewOfFile(dstbuf), "Unmapping file view");

    WIN_CHECK(0 != CloseHandle(filemap), "Closing file mapping");

    MPI_CHECK(MPI_Barrier(comm));

    if (rank == 0)
    {
	const size_t filesize = finalsize;
	long filesizehi = (long)(filesize >> 32);

	WIN_CHECK(INVALID_SET_FILE_POINTER != SetFilePointer(fd, filesize & (uint32_t)0xffffffff, &filesizehi, FILE_BEGIN), "Set file pointer");
	WIN_CHECK(0 != SetEndOfFile(fd), "Truncating the file");
	WIN_CHECK(0 != CloseHandle(fd), "Closing output file");
    }
#endif

    const double tend = MPI_Wtime();
    
    MPI_CHECK(MPI_Finalize());

    if (rank == 0)
	printf("TTS %.3fs   %.3f GiB   %.3f GiB/s\n"
	       "all good here. bye.\n",
	       tend - tstart, finalsize / double(1<<30), finalsize / (tend - tstart) / (1 <<30));
    
    return 0;
}
