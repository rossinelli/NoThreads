#pragma once

#include <cstdio>
#include <cstdlib>
#include <cerrno>
#include <cstring>

#define SYS_CHECK(stmt, str)						\
    do									\
    {									\
	if (!(stmt))							\
	{								\
	    fprintf(stderr,						\
		    "OS ERROR MESSAGE: %s\n", strerror(errno));		\
	    								\
	    fprintf(stderr,						\
		    "ERROR: "						\
		    #stmt);						\
									\
	    fprintf(stderr,						\
		    "\n%s in file* %s at line %d\n"			\
		    "\nexiting now.\n",					\
		    str, __FILE__, __LINE__);				\
									\
	    fflush(stderr);						\
									\
	    exit(EXIT_FAILURE);						\
	}								\
    }									\
    while(0)
