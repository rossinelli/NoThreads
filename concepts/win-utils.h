#pragma once

#include <string>

//Returns the last Win32 error, in string format. Returns an empty string if there is no error.
std::string GetLastErrorAsString()
{
    //Get the error message, if any.
    DWORD errorMessageID = ::GetLastError();
    if(errorMessageID == 0)
        return std::string(); //No error message has been recorded

    LPSTR messageBuffer = nullptr;
    size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                                 NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

    std::string message(messageBuffer, size);

    //Free the buffer.
    LocalFree(messageBuffer);

    return message;
}

#define WIN_CHECK(stmt, str)					\
    do								\
    {								\
	if (!(stmt))						\
	{							\
	    fprintf(stderr,					\
		    "ERROR %d: %s\n"				\
		    "with %s in file %s at line %d\n"		\
		    "exiting now.\n",				\
		    GetLastError(),				\
		    GetLastErrorAsString().c_str(),		\
		    str, __FILE__, __LINE__);			\
								\
	    exit(EXIT_FAILURE);					\
	}							\
    }								\
    while(0)
