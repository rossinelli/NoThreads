#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <stdint.h>

#include "mpi-utils.h"
#include "std-utils.h"

#include <sched.h>

#ifdef _WIN32
#include <windows.h>
#endif

int get_policy()
{
#ifdef __linux__
    int policy;
    SYS_CHECK(-1 != (policy = sched_getscheduler(0)), "get scheduler");

    switch(policy)
    {
    case SCHED_OTHER:
	printf("SCHED_OTHER\n");
	break;
    case SCHED_BATCH:
	printf("SCHED_BATCH\n");
	break;
    case SCHED_IDLE:
	printf("SCHED_IDLE\n");
	break;
    default:
	printf("UNKNOWN! \n");
    }

    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));
    
    return policy;
#else
    return 0;
#endif
}

int main(int argc, char ** argv)
{
    MPI_CHECK(MPI_Init(&argc, &argv));
   
    int rank, nranks;
    MPI_CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank));
    MPI_CHECK(MPI_Comm_size(MPI_COMM_WORLD, &nranks));
#ifdef __linux__
    int policy = get_policy();

    sched_param param;
    param.sched_priority = 0;

    int pmin, pmax;
    SYS_CHECK(-1 != (pmax = sched_get_priority_max(policy)), "get priority min val");
    SYS_CHECK(-1 != (pmin = sched_get_priority_min(policy)), "get priority min val");
    assert(pmin == 0 && pmax == 0);
    
    SYS_CHECK(-1 != sched_setscheduler(0, SCHED_BATCH, &param), "set sched");
    
    policy = get_policy();
#elif defined(_WIN32)
    HANDLE tid = GetCurrentThread();
    SYS_CHECK(0 != SetThreadPriority(tid, THREAD_PRIORITY_ABOVE_NORMAL), "priority");
    assert(THREAD_PRIORITY_ABOVE_NORMAL == GetThreadPriority(GetCurrentThread()));
#endif
    MPI_CHECK(MPI_Finalize());

    if (rank == 0)
	printf("all good here. bye.\n");
    
    return 0;
}
