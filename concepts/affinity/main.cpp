#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <stdint.h>

#include <sched.h>
#ifdef __linux__
#include <numa.h>
#elif defined(_WIN32)
#include <windows.h>
#endif
#include <unistd.h>

#include "mpi-utils.h"
#include "std-utils.h"

int32_t hash_node()
{
    int nodenamelen;
    char nodename[2048];
    MPI_CHECK(MPI_Get_processor_name(nodename, &nodenamelen));

    //compute hash
    unsigned char * str = (unsigned char *)nodename;
    unsigned int hash = 5381;
    int32_t c;

    while ((c = *str++))
        hash = ((hash << 5) + hash) + c;

    //msb shave
    hash = (uint32_t)(hash & ~((int64_t)1 << (int64_t)31));

    return hash;
}

int pin_to_hwt(MPI_Comm nodecomm)
{
    int hwtid;

    {
#ifdef __linux__
	hwtid = sched_getcpu();
#elif defined(_WIN32)
	hwtid = GetCurrentProcessorNumber();
#endif
	int nranks, rank;
	MPI_CHECK(MPI_Comm_size(nodecomm, &nranks));
	MPI_CHECK(MPI_Comm_rank(nodecomm, &rank));

	int allids[nranks];
	MPI_CHECK(MPI_Allgather(&hwtid, 1, MPI_INT, allids, 1, MPI_INT, nodecomm));

	int unique = 1;
        for(int i = 0; i < nranks; ++i)
	    unique = unique && (allids[i] != hwtid || i == rank);

	MPI_CHECK(MPI_Allreduce(MPI_IN_PLACE, &unique, 1, MPI_INT, MPI_MIN, nodecomm));

	if (!unique)
	{
	    if (rank == 0)
	    {
		fprintf(stderr,
			"hwtid overwritten! ");
		for(int i = 0; i < nranks; ++i)
		    fprintf(stderr, "%d ", allids[i]);

		fprintf(stderr, "\n");

		fflush(stderr);
	    }

	    hwtid = rank;
	}
    }
#ifdef __linux__
    cpu_set_t mask;
    CPU_ZERO(&mask);
    CPU_SET(hwtid, &mask);

    const int nhwts = sysconf(_SC_NPROCESSORS_ONLN);
    SYS_CHECK(0 == sched_setaffinity(getpid(), nhwts, &mask), "setting affinity");
#elif defined(_WIN32)
    uint64_t mask = 0;
    assert(hwtid < 64 && hwtid >= 0);
    mask |= (uint64_t)1 << hwtid;
    SYS_CHECK(0 != SetProcessAffinityMask(GetCurrentProcess(), mask), "set affinity mask");
#endif
    return hwtid;
}

void print_comm_name(MPI_Comm comm)
{
    char comm_name[MPI_MAX_OBJECT_NAME];
    int len;
    MPI_CHECK(MPI_Comm_get_name(comm, comm_name, &len));
    comm_name[len] = 0;

    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));

    printf("COMM NAME: %s %zd %d\n", comm_name, sizeof(comm_name), len);
    fflush(stdout);

    MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));
}

void setname_comm(MPI_Comm parentcomm,
		  MPI_Comm comm,
		  const char name[],
		  const int key)
{
    char parentname[MPI_MAX_OBJECT_NAME];
    int len;
    MPI_CHECK(MPI_Comm_get_name(parentcomm, parentname, &len));
    parentname[len] = 0;

    char fullname[MPI_MAX_OBJECT_NAME];
    sprintf(fullname, "%s/%s%x", parentname, name, key);

    MPI_CHECK(MPI_Comm_set_name(comm, fullname));
}

int main(int argc, char ** argv)
{
    MPI_CHECK(MPI_Init(&argc, &argv));

    int rank, nranks;
    MPI_CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank));
    MPI_CHECK(MPI_Comm_size(MPI_COMM_WORLD, &nranks));

    print_comm_name(MPI_COMM_WORLD);

    const int nodehash = hash_node();

    MPI_Comm nodecomm;
    MPI_CHECK(MPI_Comm_split(MPI_COMM_WORLD, nodehash, rank, &nodecomm));
    
    setname_comm(MPI_COMM_WORLD, nodecomm, "node", nodehash);
    print_comm_name(nodecomm);

    int noderank, noderanks;
    MPI_CHECK(MPI_Comm_rank(nodecomm, &noderank));
    MPI_CHECK(MPI_Comm_size(nodecomm, &noderanks));

    const int hwtid = pin_to_hwt(nodecomm);
#ifdef __linux__
    const int numaid = numa_node_of_cpu(hwtid);
#elif defined(_WIN32)
    int numaid;
    {
	UCHAR result;
	SYS_CHECK(0 != GetNumaProcessorNode(hwtid, &result), "get numa node");
	numaid = result;
    }
#else
    int numaid = 0;
#endif
    MPI_Comm numacomm;
    MPI_CHECK(MPI_Comm_split(nodecomm, numaid, noderank, &numacomm));
    
    setname_comm(nodecomm, numacomm, "numa", numaid);
    print_comm_name(numacomm);

    int numarank, numaranks;
    MPI_CHECK(MPI_Comm_rank(numacomm, &numarank));
    MPI_CHECK(MPI_Comm_size(numacomm, &numaranks));

    int coreid = 0;

#ifdef __linux__
    //const int
    {
	char fname[2048];
	sprintf(fname, "/sys/devices/system/cpu/cpu%d/topology/thread_siblings_list", hwtid);
	FILE * f = fopen(fname, "r");

	assert(f);
	
	int s[8];
	const int n = fscanf(f, "%d,%d,%d,%d,%d,%d,%d,%d",
			     s + 0, s + 1, s + 2, s + 3,
			     s + 4, s + 5, s + 6, s + 7);

	assert(n);
	//printf("i have found %d siblings\n", n);

	coreid = s[0];

	fclose(f);
    }
#elif defined(_WIN32)
    {
        DWORD len = 0;
	GetLogicalProcessorInformation(NULL, &len);

	const int n = len / sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
	SYSTEM_LOGICAL_PROCESSOR_INFORMATION buf [n];
	assert(len %  sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) == 0);

	SYS_CHECK(0 != GetLogicalProcessorInformation(buf, &len), "get processor info");

	bool found = false;
	int corecount = 0;
	for(int i = 0; i < n; ++i)
	    if (buf[i].Relationship == RelationProcessorCore)
	    {
		for(int j = 0; j < 64; ++j)
		    if (buf[i].ProcessorMask & ((uint64_t)1 << j))
			if (j == hwtid)
			{
			    coreid = corecount;
			    found = true;
			    goto corefound;
			}
		
		++corecount;
	    }
    corefound:
	assert(found);
	
    }
#endif

    MPI_Comm corecomm;
    MPI_CHECK(MPI_Comm_split(numacomm, coreid, numarank, &corecomm));
    
    setname_comm(numacomm, corecomm, "core", coreid);
    print_comm_name(corecomm);

    int corerank, coreranks;
    MPI_CHECK(MPI_Comm_rank(corecomm, &corerank));
    MPI_CHECK(MPI_Comm_size(corecomm, &coreranks));
    
    printf("hello rank %d of node %x, "
	   "rank %d of %d of node comm, "
	   "rank %d of %d of numa comm, "
	   "rank %d of %d of core comm, "
	   "running on hwtid %d -> coreid %d\n",
	   rank, nodehash,
	   noderank, noderanks,
	   numarank, numaranks,
	   corerank, coreranks,
	   hwtid, coreid);

    MPI_CHECK(MPI_Barrier(nodecomm));

    MPI_CHECK(MPI_Finalize());

    return 0;
}
