#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <stdint.h>

#include <fcntl.h>
#include <unistd.h>

#if defined(__linux__) or defined(__APPLE__)
#include <sys/mman.h>
#include <sys/stat.h>
#else
#include <windows.h>
#include "win-utils.h"
#endif

#include "mpi-utils.h"
#include "std-utils.h"

struct shmem_posix
{
    size_t nbytes;
#if defined(__linux__) or defined(__APPLE__)
    int fd;
#else
    HANDLE fd;
#endif
    void * buf;
    char * name;
    MPI_Comm comm;

    shmem_posix(
	MPI_Comm comm,
	const char desired_name[],
	const size_t desired_nbytes) : //collective call
	nbytes(desired_nbytes), buf(NULL), name(NULL), comm(comm)
    {
	SYS_CHECK(NULL != (name = strdup(desired_name)), "strdup");

#ifdef __linux__

	int fd;
	MPI_CHECK(MPI_Barrier(comm));
	SYS_CHECK(-1 != (fd = shm_open(desired_name, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR)), "creating output file");
	SYS_CHECK(0 == ftruncate(fd, nbytes), "truncate out file");
	SYS_CHECK(MAP_FAILED != (buf = mmap(NULL, nbytes, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0)), "memory map");
	MPI_CHECK(MPI_Barrier(comm));

#elif defined(__APPLE__)
	/* preventive action */
	shm_unlink(name);
	errno = 0;

	int rank;
        MPI_CHECK(MPI_Comm_rank(comm, &rank));

	if (rank == 0)
	{
	    SYS_CHECK(-1 != (fd = shm_open(name, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR)),
		      "shm_open master");
	}

	MPI_CHECK(MPI_Barrier(comm));

	if (rank != 0)
	    SYS_CHECK(-1 != (fd = shm_open(name, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR)),
		      "shm_open slaves");

	MPI_CHECK(MPI_Barrier(comm));

	if (rank == 0)
	{
	    struct stat mapstat;

	    if (-1 != fstat(fd, &mapstat) && (size_t)mapstat.st_size != nbytes)
		SYS_CHECK(0 == ftruncate(fd, nbytes), "ftruncate");
	}

	buf = mmap(NULL, nbytes, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);
	SYS_CHECK(buf != MAP_FAILED, "memory map");
#else
	WIN_CHECK(fd = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, nbytes, desired_name), "Create file mapping");
	WIN_CHECK(buf = MapViewOfFile(fd, FILE_MAP_ALL_ACCESS, 0, 0, nbytes), "Map view of file roi");
#endif

	MPI_CHECK(MPI_Barrier(comm));
    }

    float * data() const { return (float *)buf; }

    ~shmem_posix() //collective call
    {
#if defined(__linux__) or defined(__APPLE__)
	SYS_CHECK(0 == munmap(buf, nbytes), "munmap");

	MPI_CHECK(MPI_Barrier(comm));

	int rank;
	MPI_CHECK(MPI_Comm_rank(comm, &rank));

	if (rank == 0)
	    SYS_CHECK(0 == shm_unlink(name), "shm_unlink");
#else
	UnmapViewOfFile(buf);
	CloseHandle(fd);
#endif
	free(name);

    }
};

#if defined(__linux__) or defined(__APPLE__)
#include <sys/shm.h>
#include <sys/resource.h>
#include <algorithm>
struct shmem_sysv
{
    enum { NMAX = 128 };

    static int ninstances;

    int memid[NMAX];
    size_t nbytes_desired, chunksize, chunks;
    void * reserved, *buf[NMAX];

    MPI_Comm comm;

    shmem_sysv(
	MPI_Comm comm,
	const size_t desired_nbytes) : //collective call
	nbytes_desired(desired_nbytes), comm(comm)
    {

	const size_t memlimit = 2ll << 30ll;
	chunksize = std::min(desired_nbytes, memlimit);
	chunks = (nbytes_desired + chunksize - 1) / chunksize;

	const long pgsz = 2048 << 10;//sysconf(_SC_PAGESIZE);

	SYS_CHECK(0 == posix_memalign((void **)&reserved, pgsz, desired_nbytes), "posix_memalign");
	free(reserved);

	for(size_t c = 0; c < chunks; ++c)
	{
	    key_t k;
	    SYS_CHECK(-1 != (k = ftok("/tmp"/*"/dev/null"*/, ninstances++)), "ftok");
	    const size_t nbytes = std::min(desired_nbytes - c * chunksize, memlimit);

	    SYS_CHECK(-1 != (memid[c] = shmget(k, nbytes, /*SHM_HUGETLB | SHM_NORESERVE | */IPC_CREAT | S_IRUSR | S_IWUSR)), "shmget");

	    void * tmp;
	    SYS_CHECK(MAP_FAILED != (tmp = shmat(memid[c], (char * )reserved + chunksize * c, 0)), "shmat");
	    buf[c] = tmp;

	    assert(tmp == (char * )reserved + chunksize * c);
	}
    }

    float * data() const { return (float *)buf[0]; }

    ~shmem_sysv()
    {
	MPI_CHECK(MPI_Barrier(comm));

	for(size_t c =0 ; c < chunks; ++c)
	    SYS_CHECK(0 == shmctl(memid[c], IPC_RMID, NULL), "shmctl");//buf[c]), "shmdt");

	MPI_CHECK(MPI_Barrier(comm));
	//fprintf(stderr, "cleanup..\n");
	for(size_t c =0 ; c < chunks; ++c)
	    SYS_CHECK(0 == shmdt(buf[c]), "shmdt");

	MPI_CHECK(MPI_Barrier(comm));

	--ninstances;
	assert(ninstances >= 0);
    }
};

int shmem_sysv::ninstances = 0;
#endif

struct shmem_mpi
{
    MPI_Comm comm;
    void * buf;
    MPI_Win win;
    size_t nbytes;

    shmem_mpi(
	MPI_Comm comm,
	const size_t nbytes) :  nbytes(nbytes), comm(comm), buf(NULL)
    {
	MPI_CHECK( MPI_Barrier(comm));

	MPI_CHECK( MPI_Win_allocate_shared(
		       nbytes, 1, MPI_INFO_NULL, comm, &buf, &win));

	MPI_CHECK( MPI_Barrier(comm));
    }

    float * data() const { return (float *)buf; }

    ~shmem_mpi()
    {
	MPI_CHECK( MPI_Barrier(comm));
	MPI_CHECK(MPI_Win_free(&win));
	MPI_CHECK( MPI_Barrier(comm));
    }
};

#include <stddef.h>
#include <signal.h>

static void handler(int sig, siginfo_t *si, void *unused)
{
    printf("FAILED!\n");

    exit(-1);
}

int main(int argc, char ** argv)
{
    struct sigaction sa;

    sa.sa_flags = SA_SIGINFO;
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = handler;
    if (sigaction(SIGBUS, &sa, NULL) == -1)
        perror("failed to set handler");

    MPI_CHECK(MPI_Init(&argc, &argv));

    MPI_Comm comm = MPI_COMM_WORLD;

    int rank, nranks;
    MPI_CHECK(MPI_Comm_rank(comm, &rank));
    MPI_CHECK(MPI_Comm_size(comm, &nranks));

    const int master = rank == 0;

    const int xsize = 4240, ysize = 3500;

    if (argc != 2)
    {
	if (master)
	    fprintf(stderr, "usage: %s <zsize>\n",
		    argv[0]);

	return EXIT_FAILURE;
    }

    const int zsize = atoi(argv[1]);

    const ptrdiff_t slicesize = xsize * ysize;

    if (master)
	fprintf(stderr, "rank %d of %d: allocating about %.fGB...\n",
		rank, nranks, slicesize * (ptrdiff_t)zsize * 4 / 1e9);

    {
#ifdef _OSC_
	shmem_mpi region(comm, sizeof(float) * (size_t)slicesize * (size_t)zsize);
#elif defined(_POSIX_)
	shmem_posix region(comm, "staging-buffer",
			   sizeof(float) * (size_t)slicesize * (size_t)zsize);

	if (master)
	    fprintf(stderr, "posix implementation\n");
#else
	shmem_sysv region(comm, sizeof(float) * slicesize * (size_t)zsize);

	if (master)
	    fprintf(stderr, "sysv implementation\n");
#endif

	float * data = region.data();

	MPI_CHECK(MPI_Barrier(comm));

	const double t0 = MPI_Wtime();

	if (master)
	    printf("writing...\n");

	MPI_CHECK(MPI_Barrier(comm));

	for(int zi = rank; zi < zsize; zi += nranks)
	    memset(data + slicesize * zi, rank, slicesize * sizeof(float));

	MPI_CHECK(MPI_Barrier(comm));

	if (master)
	    printf("verifying...\n");

	const double t1 = MPI_Wtime();

	if (master)
	{
	    for(int zi = 0; zi < zsize; ++zi)
	    {
		union { int32_t v32; uint8_t v8[4]; } val;

		val.v8[0] = val.v8[1] = val.v8[2] =  val.v8[3] = zi % nranks;

		const int * const slice = (int *)data + slicesize * zi;

		for(ptrdiff_t i = 0; i < slicesize; ++i)
		{
		    if (slice[i] != val.v32)
			printf("val: %zd %d\n", i, slice[i]);

		    assert(slice[i] == val.v32);
		}
	    }

	    printf("verified. write throughput: %.3f GB/s\n",
		   (sizeof(float) * slicesize * (size_t)zsize) / 1e9 / (t1 - t0));
	}
    }

    MPI_CHECK(MPI_Finalize());

    if (master)
	printf("all good here. bye.\n");

    return 0;
}
