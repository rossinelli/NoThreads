DIRS = library benchmarks concepts
CXX = mpicxx

all:
	for F in $(DIRS) ; do $(MAKE) -C $$F ; done

clean:
	for F in $(DIRS) ; do $(MAKE) -C $$F clean; done

.PHONY: all

